
About
=====
PayNearMe.com is a service aimed to allow e-commerce transactions for 
people who do not have (or want to use) a bank or credit card.

The PayNearMe module for Drupal's Ubercart platform allows users to 
pay for goods in Ubercart using PayNearMe.


Requirements for Installation
=============================

1. Install the Ubercart modules and configure your store.
2. Get an account on PayNearMe.com and log in to the merchant area. 


Ubercart Configuration Steps
============================

## Required
1. Download and extract the module into your modules directory, usually sites/all/modules

2. Got to Administer > Site building > Modules and enable the PayNearMe module.

3. On admin/store/settings/payment/edit/methods enable PayNearMe and update
   the settings with values from the PayNearMe.com merchant website.


## Optional:

1. On admin/store/settings/checkout/edit/basic you can disable checkout
   if you wish to run most of the orders directly through PayNearMe without 
   collecting unnecessary information on your Drupal site. This may lead to 
   increased conversion ratios if you don't need that information for the 
   purchase (i.e. people are already logged in and items are not shippable 
   and you don't have any other payment methods).


PayNearMe Gateway configuration steps
=====================================

1. You should configure the Success URL to be cart/paynearme/landing/success so
   if your site were example.com then enter: http://example.com/cart/paynearme/landing/success

2. You should conrfigure the Confirmation URL to be cart/paynearme/confirmation so
   if your site were example.com then enter: http://example.com/cart/paynearme/confirmation

3. You should configure the Pending Payment URL to be /cart/paynearme/pending so
   if your site were example.com then enter: http://example.com/cart/paynearme/pending
   
4. You can specify a failure URL like cart/paynearme/landing/success or any other
   URL like a Page node.

You should not configure an Authorization URL. If you need support for 
Authorization (i.e. realtime notification of a payment prior to allowing payment) see 
the PayNearMe module issue queue.

Module Credits
==============

This module originally developed by Growing Venture Solutions 
Greg Knaddison developed the module - http://drupal.org/user/36762
Ezra Gildesgame provided code review - http://drupal.org/user/69959 
Lisa Rex provided a usability review - http://drupal.org/user/485222

Known Problems
==============
The module assumes that all orders are in USA Dollars. This is a limitation of
PayNearMe and their retail partners being USA based companies AND a recognition
of the fact that there is no "one good way" to do currencies in Ubercart.

The module maintainers would love to have support for alternate currencies where
and how it makes sense. Please file an issue in the queue or contact us to
discuss the idea further. 
 