<?php

/**
 * @file
 * PayNearMe menu callbacks.
 *
 */

/**
 * A page callback for the user when their order is over..
 *
 * @param $status
 * @return unknown_type
 */
function paynearme_landing($status = 'success') {
  if ($status == 'success') {
    uc_cart_empty(uc_cart_get_id());
    drupal_set_message(t('Your promise to pay was successfully received on PayNearMe.com.'));
  }
  else {
    // @TODO we could include some order details for this to make it easier to debug.
    watchdog('PayNearMe', t('An order was sent back in with a problem. Check this users order history for details.'));

    drupal_set_message(t('Whoops, there was a problem completing your order on the PayNearMe site. Please contact support to fix this problem.'));
  }
  drupal_goto(variable_get('uc_cart_checkout_complete_page', '<front>'));
}

/**
 * Once an order is pending we can clear the cart contents.
 * The audience for this page is the PayNearMe postback tool (i.e. a bot).
 * @return string
 *   XML data and/or errors.
 */
function paynearme_pending() {
  // Empty the cart so they won't be surprised by the contents when they come back.
  // That way if they abandon PayNearMe as a payment method they still have the cart contents.

  // Let's try to get the order first.
  $order = uc_order_load($_GET['site_order_identifier']);

  if ($order === FALSE) {
    watchdog('PayNearMe', 'Received a postback without any order data: @request', array('@request' => print_r($_GET, true)), WATCHDOG_ERROR);
    print t('An error has occurred during payment.  Please contact us to ensure your order was submitted.');
    exit();
  }
  if (uc_order_status_data($order->order_status, 'state') != 'in_checkout') {
    watchdog('PayNearMe', 'Attempting to mark an order pending that is already beyond pending: order <a href="@order_link">#@order_id</a>', array('@order_id' => $order->order_id, '@order_link' => url('admin/store/orders/'. $order->order_id)), WATCHDOG_ERROR);
    print t('An error has occurred during pending state change.  Please contact us to ensure your order has submitted.');
    exit();
  }

  // Get the order/cart details from the request and confirm they are valid.
  $postback = paynearme_validate_request($_GET);
  if (!$postback) {
    uc_order_comment_save($order->order_id, 0, t('Attempted unverified PayNearMe pending status for this order.'), 'admin');
    watchdog('PayNearMe', 'Received a postback with a bad signature. Request: @request', array('@request' => print_r($_GET, true)), WATCHDOG_ERROR);
    return drupal_not_found();
  }

  // Clear that cart. The cart_id is generally the UID unless they are anonymous.
  if (!empty($postback['site_order_annotation'])) {
    $annotation = unserialize($postback['site_order_annotation']);
    if (!empty($annotation['cart_id'])) {
      uc_cart_empty($annotation['cart_id']);
    }
  }

  // Provide the proper response stuff.
  $output = "<t:pending_payment_response
    version='1.3'
    xsi:schemaLocation='http://www.paynearme.com/public/api/pnm_xmlschema_v1_3
    pnm_xmlschema_v1_3.xsd' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
    xmlns:t='http://www.paynearme.com/public/api/pnm_xmlschema_v1_3'>
        <t:pending_payment>
            <t:pnm_order_identifier>";

  $output .= check_plain($postback['pnm_order_identifier']);

  $output .= "</t:pnm_order_identifier>
        </t:pending_payment>
  </t:pending_payment_response>";

  print $output;

  // If we're in debug mode, watchdog it.
  if (variable_get('paynearme_debug_mode', FALSE)) {
    watchdog('PayNearMe', t('The postback data was: @postback', array('@postback' => print_r($postback, TRUE))));
    watchdog('PayNearMe', t('The order pending xml response was: @output', array('@output' => print_r($output, TRUE))));
  }

  exit();
}

/**
 * Responds to notifications that payment has been received.
 * The audience for this page is the PayNearMe postback tool (i.e. a bot).
 * @return string
 *   XML data and/or errors.
 */
function paynearme_confirm() {
  if (variable_get('paynearme_debug_mode', FALSE)) {
    watchdog('PayNearMe', t('The postback was: @postback', array('@postback' => print_r($_GET, TRUE))));
  }

  // Let's try to get the order first.
  $order = uc_order_load($_GET['site_order_identifier']);

  if ($order === FALSE) {
    watchdog('PayNearMe', 'Received a postback without any order data: @request', array('@request' => print_r($_GET, true)), WATCHDOG_ERROR);
    print t('An error has occurred during payment.  Please contact us to ensure your order has submitted.');
    exit();
  }
  if (uc_order_status_data($order->order_status, 'state') != 'in_checkout') {
    watchdog('PayNearMe', 'Attempting to pay for an order that is already paid: order <a href="@order_link">#@order_id</a>', array('@order_id' => $order->order_id, '@order_link' => url('admin/store/orders/'. $order->order_id)), WATCHDOG_ERROR);
    print t('An error has occurred during payment.  Please contact us to ensure your order has submitted.');
    exit();
  }

  // Get the parameters and confirm they are real.
  $postback = paynearme_validate_request($_GET);
  if (!$postback) {
    uc_order_comment_save($order->order_id, 0, t('Attempted unverified PayNearMe completion for this order.'), 'admin');
    watchdog('PayNearMe', 'Received a postback with a bad signature. Request: @request', array('@request' => print_r($_GET, true)), WATCHDOG_ERROR);
    return drupal_not_found();
  }

  // Just keeping track of what's going on.
  watchdog('PayNearMe', 'Receiving new order notification for order: <a href="@order_link">#@order_id</a>.', array('@order_id' => $order->order_id, '@order_link' => url('admin/store/orders/'. $order->order_id)));

  // @TODOLOW: if they have variable_get('paynearme_address_on_gateway', FALSE) then we should be getting the billing info from the gateway. We don't have that ability currently.
//  $order->billing_street1 = $_GET['street_address'];
//  $order->billing_street2 = $_GET['street_address2'];
//  $order->city = $_GET['city'];
//  $order->billing_postal_code = $_GET['zip'];
//  $order->billing_phone = $_GET['phone'];

//  $zone_id = db_result(db_query("SELECT zone_id FROM {uc_zones} WHERE zone_code LIKE '%s'", $_GET['state']));
//  if (!empty($zone_id)) {
//    $order->billing_zone = $zone_id;
//  }
//
//  $country_id = db_result(db_query("SELECT country_id FROM {uc_countries} WHERE country_name LIKE '%s'", $_GET['country']));
//  if (!empty($country_id)) {
//    $order->billing_country = $country_id;
//  }
//
//  if (strtolower($_GET['email']) !== strtolower($order->primary_email)) {
//    uc_order_comment_save($order->order_id, 0, t('Customer used a different e-mail address during payment: !email', array('!email' => check_plain($_GET['email']))), 'admin');
//  }

  // Add a comment to let sales team know this came in through the site.
  uc_order_comment_save($order->order_id, 0, t('Order created through website.'), 'admin');

  if (is_numeric($_GET['funded_amount']) && $_GET['status'] == 'payment') {

    // @todolow: some day confirm that the currency matches the order currency if the site uses multiple currencies.
    $comment = t('PayNearMe.com order #@order paid.', array('@order' => $order->order_id));
    uc_payment_enter($order->order_id, 'paynearme', $_GET['funded_amount'], 0, NULL, $comment);

    $output = "<t:payment_confirmation_response
    version='1.3'
    xsi:schemaLocation='http://www.paynearme.com/public/api/pnm_xmlschema_v1_3 pnm_xmlschema_v1_3.xsd'
    xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
    xmlns:t='http://www.paynearme.com/public/api/pnm_xmlschema_v1_3'>
          <t:confirmation>
            <t:pnm_order_identifier>";
    $output .= check_plain($postback['pnm_order_identifier']);
    $output .= "</t:pnm_order_identifier>
        </t:confirmation>
      </t:payment_confirmation_response>";

    // Save any changes to the order.
    uc_order_save($order);

    // Call this with FALSE for login. Remember, it's a bot who hits this callback.
    uc_cart_complete_sale($order, FALSE);

    // Watchdog the successes too.
    watchdog('PayNearMe', 'Successfully received payment message from PayNearMe.com for order: <a href="@order_link">#@order_id</a>.', array('@order_id' => $order->order_id, '@order_link' => url('admin/store/orders/'. $order->order_id)));
  }
  else {
    watchdog('PayNearMe', 'Received a postback with an incorrect funded_amount or incorrect status (this is not common). Request: @request', array('@request' => print_r($_GET, true)), WATCHDOG_ERROR);
    uc_order_comment_save($order->order_id, 0, t('Payment was incomplete at PayNearMe.com (this is not common). Request: @request', array('@request' => print_r($_GET, true))), 'admin');
    $output = 'An error occurred while recording payment.';

    // Save any changes to the order.
    uc_order_save($order);
  }

  print $output;
  if (variable_get('paynearme_debug_mode', FALSE)) {
    watchdog('PayNearMe', t('The order payment xml response was: @output', array('@output' => print_r($output, TRUE))));
  }

  exit();
}

/**
 * Helper function to parse a GET request, validate the signature, and return
 * either the keys or FALSE if it does not validate.
 *
 * @return array of parameters or FALSE if the signature fails.
 */
function paynearme_validate_request($get) {

  // Collect the variables and validate the signature.
  unset($get['q']);
  $postback = array();
  $request_string  = '';
  ksort($get);
  foreach ($get as $key => $value) {
    $postback[$key] = $value;
    if ($key != 'signature') {
      $request_string .= $key . $value;
    }
  }
  $calculated_signature = md5($request_string .variable_get('paynearme_secret_key', ''));

  // If the signature doesn't match, break out.
  if (!empty($postback['signature']) && $calculated_signature == $postback['signature']) {
    return $postback;
  }
  return FALSE;
}
